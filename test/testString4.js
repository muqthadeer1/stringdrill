// create a variable to import a function from other directory
let fullNameFn = require("../string4.js");

// create a variable to execute the function which is imported
let getFullName = fullNameFn({
  first_name: "JoHN",
  middle_name: "doe",
  last_name: "SMith",
});
 
// prints the output
console.log(getFullName);
