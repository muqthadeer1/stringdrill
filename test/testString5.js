// create variable stringArrayAddedFn to import the function from other directory
let stringArrayAddedFn = require("../string5.js");

//create a variable stringSentence to execute the function
let stringSentence = stringArrayAddedFn(["the", "quick", "brown", "fox"]);

//it prints the output
console.log(stringSentence);
 