//create function to add string arrays
function fnAddStringArray(string) {
  // if empty string  it return empty Array
  if (string.length == 0) {
    return [];
  }
  // if not empty string
  else {
    let joinedArray = string.join(" ");

    return joinedArray + ".";
  }
}

// create a module to export the function named fnAddStringArray
module.exports = fnAddStringArray;
