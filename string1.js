// create the function to convert string to number;
function convertString(string) {
  //create the variable to store the first elements
  let subString = string.slice(0, 2);

  // create a variable to store output
  let finalOutput;

  // iterate the loop upto two elements whether element  is "-" or "$"
  for (let index = 0; index < subString.length; index++) {
    //if the first element is "$"
    if (subString[0] === "$") {
      finalOutput = string.slice(1, string.length);
    }
    // if the firts element is "-"
    else if (subString[0] === "-") {
      finalOutput = string.slice(0, 1) + string.slice(2, string.length);
    }
  }
  //if it is not a number
  if (isNaN(finalOutput)) {
    return 0;
  }
  // if it is a number
  else {
    return Number(finalOutput);
  }
}

// create module to exports the function to other file
module.exports = convertString;
