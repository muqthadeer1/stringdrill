// create the function to convert the String to number array

function convertStringToNumber(string) {
  //create a variable to store Array of string
  let withOutDotString = string.split(".");

  //create a variable with empty Array
  let convertStringToNumber = [];

  //loop for iterating the total value of string
  for (let index = 0; index < withOutDotString.length; index++) {
    // each string value converts into number and stores in variable.
    convertStringToNumber.push(Number(withOutDotString[index]));
  }

  return convertStringToNumber;
}

// create a module to exports the function to other file
module.exports = convertStringToNumber;
