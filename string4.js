//create a function to get full name from string object
function fnFullName(object) {
  //create a variable with Empty string
  let fullName = "";

  //if empty string
  if (object.length == 0) {
    return fullName;
  }
  // if not Empty String
  else {
    for (let key in object) {
      fullName += object[key] + " ";
    }
    return fullName;
  }
}

//create a module to export the function to other files
module.exports = fnFullName;
