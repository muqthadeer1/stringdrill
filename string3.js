// create a function to find month from given date
function findMonth(string) {
  //create a variable subString store date in seperate day,month,year
  let subString = string.split("/");

  // if date is not a number
  if (isNaN(subString[1])) {
    return "Not Valid Month";
  }
  //if month is valid
  else {
    return subString[1];
  }
}
// create module to  export the function to other file
module.exports = findMonth;
